# Rapid development with Boilerplate Gulp Sass Babel
Hurry up don't waste your time, Do these commands in your terminal.

# Install
```bash
$ npm install
$ gulp dev
$ gulp build
```

# Notes
1- gulp dev will start a development server and will watch for changes
2- gulp build will build production files in build folder

# Features
1- Support Gulp (ver=3.9)  
2- Support Sass(SCSS)  
3- Support ES6+ (Babel)  
4- Support BrowserSync  
5- Support Browserify (So You can write module import/export in your js file)  
6- Live reload any changes in (html, scss, js files), prevent crash while coding  
